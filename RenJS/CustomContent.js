RenJS.customContent = {
	//put here your own functions

	minigame: function () {
		console.log("Starting minigame");
		var canvas = document.querySelector("canvas");
		canvas.style.display="none";
		createMinigame(function(score){
			console.log("Minigame is over")
			RenJS.logicManager.vars.score = score;
			canvas.style.display="block";
			RenJS.resolve();
		})
	}
}

